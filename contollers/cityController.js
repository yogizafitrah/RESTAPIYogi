import City from "../models/City.js";

export const getCities = async (req, res) => {
    try {
        const cities = await City.find();
        res.json(cities);
    } catch (error) {
        res.status(200).json({message: error.message});
    }
}
export const saveCity = async (req, res) => {
    const city = new City(req.body);
    try {
        const savedCities = await city.save();
        res.status(201).json(savedCities);
    } catch (error) {
        res.status(400).json({message: error.message});
    }
}

export const updateCity = async (req, res) => {
    const cekId = await City.findById(req.params.id);
    if(!cekId) return res.status(404).json({message: "Data tidak ditemukan "});
    try {
        const updatedCities = await City.updateOne({_id: req.params.id},{$set: req.body});
        res.status(200).json(updatedCities);
    } catch (error) {
        res.status(400).json({message: error.message});
    }
}

export const getCityById = async (req, res) => {
    try {
        const city = await City.findById(req.params.id);
        res.json(city);
    } catch (error) {
        res.status(404).json({message: error.message});
    }
}

export const deleteCity = async (req, res) => {
    const cekId = await City.findById(req.params.id);
    if(!cekId) return res.status(404).json({message: "Data tidak ditemukan "});
    try {
        const deletedCity = await City.deleteOne({_id: req.params.id});
        res.status(200).json(deletedCity);
    } catch (error) {
        res.status(400).json({message: error.message});
    }
}