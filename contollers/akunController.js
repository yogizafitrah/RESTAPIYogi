import Akun from "../models/Akun.js";
import City from "../models/City.js";
import { default as bcrypt } from 'bcryptjs';
import validateRegisterInput from "../validation/register.js";

export const register = async(req,res) =>{
    try {
        const {errors,isValid} = validateRegisterInput(req.body);

         if(!isValid){
        return res.status(403).json(errors);
        }
        Akun.findOne({email: req.body.email})
            .then(akun => {
            if(akun){
                return res.status(400).json({'email' : 'Alamat email sudah digunakan'});
            }else{
                const newAkun = new Akun({
                    name : req.body.name,
                    email : req.body.email,
                    password : req.body.password,
                    match_password : req.body.match_password,
                    address : req.body.address,
                    city : req.body.city,
                    hobbies : req.body.hobbies
                });
                bcrypt.genSalt(10,function(err,salt) {
                    bcrypt.hash(newAkun.password,salt,(err,hash) => {
                        if(err) throw err;
                        newAkun.password = hash;
                        newAkun.save()
                            .then(akun => res.json(akun))
                            .catch(err => console.log(err))
                        return res.status(201).json(newAkun);
                    })
                });
            }
        })
    } catch (error) {
        res.status(500).json({message: error.message});
    }
}
    
export const Login = async (req,res) => {
    
}

export const getAkun = async (req, res) => {
    try {
        const akuns = await Akun.find();
        res.json(akuns);
    } catch (error) {
        res.status(500).json({message: error.message});
    }
}

export const updateAkun = async (req, res) => {
    const cekId = await Akun.findById(req.params.id);
    if(!cekId) return res.status(404).json({message: "Data tidak ditemukan "});
    try {
        const updatedAkuns = await Akun.updateMany({_id: req.params.id},{$set: req.body});
        res.status(200).json(updatedAkuns);
    } catch (error) {
        res.status(400).json({message: error.message});
    }
}

export const getAkunById = async (req, res) => {
    try {
        const akun = await Akun.findById(req.params.id);
        res.json(akun);
    } catch (error) {
        res.status(404).json({message: error.message});
    }
}

export const deleteAkun = async (req, res) => {
    const cekId = await Akun.findById(req.params.id);
    if(!cekId) return res.status(404).json({message: "Data tidak ditemukan "});
    try {
        const deletedAkun = await City.deleteOne({_id: req.params.id});
        res.status(200).json(deletedAkun);
    } catch (error) {
        res.status(400).json({message: error.message});
    }
}
