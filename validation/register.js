// const validator = require("validator");
import validator from "validator";
// import isEmpty from "../validation/is-empty";

const isEmpty = value =>
value === undefined ||
value === null ||
(typeof value === 'object' && Object.keys(value).length === 0) ||
(typeof value === 'string' && value.trim().length === 0);

export default function validateRegisterInput(data){
    let errors = {};

    data.name = !isEmpty(data.name) ? data.name:"" ; 
    data.email = !isEmpty(data.email) ? data.email:"" ; 
    data.password = !isEmpty(data.password) ? data.password:"" ; 
    data.match_password = !isEmpty(data.match_password) ? data.match_password:"" ; 
    data.address = !isEmpty(data.address) ? data.address:"" ; 
    data.city = !isEmpty(data.city) ? data.city:"" ; 
    data.hobbies = !isEmpty(data.hobbies) ? data.hobbies:"" ; 

    if(!validator.isLength(data.name,{min:3,max:50})){
        errors.name = "Nama Harus diantara 3 dan 50 Karakter";
    }

    if(!validator.isLength(data.password,{min:6,max:50})){
        errors.password = "Password minimal 6 karakter";
    }


    if(validator.isEmpty(data.name)){
        errors.name = "Data nama dibutuhkan";
    }
    
    if(validator.isEmpty(data.email)){
        errors.email = "Data Email dibutuhkan";
    }

    if(!validator.isEmail(data.email)){
        errors.email = "Email tidak valid";
    }
    if(validator.isEmpty(data.address)){
        errors.address = "Data address dibutuhkan";
    } 
    if(validator.isEmpty(data.city)){
        errors.city = "Data Kota dibutuhkan";
    } 
    if(validator.isEmpty(data.hobbies)){
        errors.hobbies = "Data Password dibutuhkan";
    } 

    if(validator.isEmpty(data.password)){
        errors.password = "Data Password dibutuhkan";
    }   

    if(!validator.equals(data.match_password,data.password)){
        errors.match_password = "Data password dan confirmed password harus sama";
    }

    if(validator.isEmpty(data.match_password)){
        errors.match_password = "Data Confirm Password dibutuhkan";
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}