import mongoose from "mongoose";

const Akun = mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    password:{
        type: String,
        required: true
    },
    match_password:{
        type: String,
        required: true
    },
    address:{
        type: String,
        required: true
    },
    city:{
        type: String,
        required: true
    },
    hobbies:{
        type: String,
        required: true
    },
    created_at:{
        type:Date,
        default:Date.now
    },
    last_login:{
        type: Date
    },
    last_update:{
        type: Date
    }


});

export default mongoose.model('Akuns', Akun);