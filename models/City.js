import mongoose from "mongoose";

const City = mongoose.Schema({
    kota:{
        type: String,
        required: true
    }
});

export default mongoose.model('Cities', City);