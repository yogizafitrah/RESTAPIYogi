import express from "express";
import {register,getAkun,getAkunById,deleteAkun,updateAkun} from "../contollers/akunController.js";

const router = express.Router();

router.get('/', getAkun);
router.get('/:id',getAkunById);
router.post('/', register);
router.patch('/:id', updateAkun);
router.delete('/:id', deleteAkun);

export default router;