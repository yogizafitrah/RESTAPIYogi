import express from "express";
import { getCities,saveCity,updateCity,getCityById ,deleteCity} from "../contollers/cityController.js";

const router = express.Router();

router.get('/', getCities);
router.get('/:id', getCityById);
router.post('/', saveCity);
router.patch('/:id', updateCity);
router.delete('/:id',deleteCity);


export default router;