import express from "express";
import passport from "passport";
import bodyParser from "body-parser";
import LocalStrategy from "passport-local";
import passportLocalMongoose from 'passport-local-mongoose';

const router = express.Router();

router.get("/", function (req, res) {
    res.render("home");
});
  
// Showing secret page
router.get("/secret", isLoggedIn, function (req, res) {
    res.render("secret");
});
  
// Showing register form
router.get("/register", function (req, res) {
    res.render("register");
});
  
  
//Showing login form
router.get("/login", function (req, res) {
    res.render("login");
});
  
//Handling user login
router.post("/login", passport.authenticate("local", {
    successRedirect: "/secret",
    failureRedirect: "/login"
}), function (req, res) {
});
  
//Handling user logout 
router.get("/logout", function (req, res) {
    req.logout();
    res.redirect("/");
});
  
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) return next();
    res.redirect("/login");
}
export default router;